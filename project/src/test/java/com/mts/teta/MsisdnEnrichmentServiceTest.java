package com.mts.teta;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MsisdnEnrichmentServiceTest {

    SearchUserService searchUserService = new SearchUserService();
    UserEnrichmentService enrichmentService = new UserEnrichmentService(searchUserService);
    MsisdnEnrichmentService msisdnEnrichmentService = new MsisdnEnrichmentService(new MsisdnMessageValidator(), enrichmentService, new ResultLogService());

    @Test
    void successEnrichment() throws Exception {
        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";


        String result = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"Vasya\",\n" +
                "        \"lastName\": \"Ivanov\"\n" +
                "    }\n" +
                "}";

        String enrichResult = msisdnEnrichmentService.enrich(inputMessage);
        JSONAssert.assertEquals(result, enrichResult, JSONCompareMode.STRICT);
        List<String> successList = msisdnEnrichmentService.getSuccessEnrichedList();
        assertEquals(inputMessage, successList.get(0));
    }

    @Test
    void errorEnrichment() throws Exception {
        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\"\n" +
                "}";


        String result = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\"\n" +
                "}";

        String enrichResult = msisdnEnrichmentService.enrich(inputMessage);
        JSONAssert.assertEquals(result, enrichResult, JSONCompareMode.STRICT);
        List<String> errorList = msisdnEnrichmentService.getErrorEnrichedList();
        assertEquals(inputMessage, errorList.get(0));
    }

    @Test
    void notJsonEnrichment() throws Exception {
        String inputMessage = "aaa";

        String enrichResult = msisdnEnrichmentService.enrich(inputMessage);
        JSONAssert.assertEquals(inputMessage, enrichResult, JSONCompareMode.STRICT);
        List<String> errorList = msisdnEnrichmentService.getErrorEnrichedList();
        assertEquals(inputMessage, errorList.get(0));
    }
}
