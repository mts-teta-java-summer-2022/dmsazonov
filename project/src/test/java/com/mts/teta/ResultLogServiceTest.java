package com.mts.teta;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResultLogServiceTest {

    ResultLogService resultLogService = new ResultLogService();

    @Test
    void successListTest(){
        resultLogService.addSuccess("aaa");
        assertEquals(1, resultLogService.getSuccessEnrichedList().size());
        assertEquals("aaa",resultLogService.getSuccessEnrichedList().get(0));
    }

    @Test
    void errorListTest(){
        resultLogService.addError("bbb");
        assertEquals(1, resultLogService.getErrorEnrichedList().size());
        assertEquals("bbb",resultLogService.getErrorEnrichedList().get(0));
    }
}
