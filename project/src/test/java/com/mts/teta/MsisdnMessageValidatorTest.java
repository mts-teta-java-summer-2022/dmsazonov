package com.mts.teta;

import com.google.gson.JsonObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MsisdnMessageValidatorTest {

    MsisdnMessageValidator validator = new MsisdnMessageValidator();

    @Test
    void assertInvalidJSONInput() {
        String test = "dddddd";
        Throwable thrown = assertThrows(IllegalStateException.class, () -> validator.validate(test));
        assertNotNull(thrown.getMessage());
    }

    @Test
    void assertNonFullJSONInput() {
        String test = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\"\n" +
                "}";

        Throwable thrown = assertThrows(Exception.class, () -> validator.validate(test));
        assertNotNull(thrown.getMessage());
    }

    @Test
    void assertNonValidJSONInput(){
        String test = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \\n" +
                "}";
        Throwable thrown = assertThrows(Exception.class, () -> validator.validate(test));
        assertNotNull(thrown.getMessage());
    }

    @Test
    void assertValidJSONInput() throws Exception {
        String test = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";
        JsonObject jsonObject = validator.validate(test);
        assertTrue(jsonObject.isJsonObject());
        assertEquals("88005553535", jsonObject.get("msisdn").getAsString());
    }
}
