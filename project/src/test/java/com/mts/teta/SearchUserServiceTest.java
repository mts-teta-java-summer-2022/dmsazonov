package com.mts.teta;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SearchUserServiceTest {

    SearchUserService searchUserService = new SearchUserService();

    @Test
    void assertNotFoundClient(){
        assertNull(searchUserService.findUser("aaa"));
    }

    @Test
    void asserFoundClient(){
        assertNotNull(searchUserService.findUser("111"));
        assertEquals("Ivan", searchUserService.findUser("111").getFirstName());
        assertEquals("Ivanov", searchUserService.findUser("111").getLastName());
    }

}
