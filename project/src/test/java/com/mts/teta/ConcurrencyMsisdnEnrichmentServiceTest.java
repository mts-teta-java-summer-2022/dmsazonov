package com.mts.teta;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConcurrencyMsisdnEnrichmentServiceTest {

    MsisdnEnrichmentService msisdnEnrichmentService = new MsisdnEnrichmentService(new MsisdnMessageValidator(), new UserEnrichmentService(new SearchUserService()), new ResultLogService());

    @Test
    void concurrencyTestNotJson() throws InterruptedException {
        int numberOfThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try {
                    String enrichResult = msisdnEnrichmentService.enrich("aaaa");
                    assertEquals("aaaa", enrichResult);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
        List<String> resultList = msisdnEnrichmentService.getErrorEnrichedList();
        assertEquals(10, resultList.size());

    }

    @Test
    void concurrencyTestRightJson() throws InterruptedException {

        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";


        String result = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"Vasya\",\n" +
                "        \"lastName\": \"Ivanov\"\n" +
                "    }\n" +
                "}";


        int numberOfThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try {
                    String enrichResult = msisdnEnrichmentService.enrich(inputMessage);
                    JSONAssert.assertEquals(result, enrichResult, JSONCompareMode.STRICT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
        List<String> resultList = msisdnEnrichmentService.getSuccessEnrichedList();
        assertEquals(10, resultList.size());
    }

}
