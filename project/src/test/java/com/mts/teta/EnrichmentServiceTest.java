package com.mts.teta;

import com.mts.teta.model.Message;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class EnrichmentServiceTest {

    EnrichmentService enrichmentService = new EnrichmentService(new EnrichmentFactoryMapper());
    @Test
    void successEnrichment() throws Exception {
        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";


        String result = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"Vasya\",\n" +
                "        \"lastName\": \"Ivanov\"\n" +
                "    }\n" +
                "}";

        String enrichResult = enrichmentService.enrich(new Message(inputMessage, Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(result, enrichResult, JSONCompareMode.STRICT);
    }

    @Test
    void errorEnrichment() throws Exception {
        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\"\n" +
                "}";

        String enrichResult = enrichmentService.enrich(new Message(inputMessage, Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(inputMessage, enrichResult, JSONCompareMode.STRICT);
    }

    @Test
    void errorNoDataEnrichment() throws Exception {
        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"111111111111\"\n" +
                "}";

        String enrichResult = enrichmentService.enrich(new Message(inputMessage, Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(inputMessage, enrichResult, JSONCompareMode.STRICT);
    }

    @Test
    void noJsonEnrichment() throws Exception {
        String inputMessage = "aaa";

        String enrichResult = enrichmentService.enrich(new Message(inputMessage, Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(inputMessage, enrichResult, JSONCompareMode.STRICT);
    }
}
