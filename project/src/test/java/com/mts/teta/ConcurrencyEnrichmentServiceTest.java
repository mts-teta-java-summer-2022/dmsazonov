package com.mts.teta;

import com.mts.teta.model.Message;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConcurrencyEnrichmentServiceTest {

    EnrichmentService enrichmentService = new EnrichmentService(new EnrichmentFactoryMapper());

    @Test
    void concurrencyTestNotJson() throws InterruptedException {
        int numberOfThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try {
                    String enrichResult = enrichmentService.enrich(new Message("aaaa", Message.EnrichmentType.MSISDN));
                    assertEquals("aaaa", enrichResult);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
    }

    @Test
    void concurrencyTestRightJson() throws InterruptedException {

        String inputMessage = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";


        String result = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"Vasya\",\n" +
                "        \"lastName\": \"Ivanov\"\n" +
                "    }\n" +
                "}";


        int numberOfThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try {
                    String enrichResult = enrichmentService.enrich(new Message(inputMessage, Message.EnrichmentType.MSISDN));
                    JSONAssert.assertEquals(result, enrichResult, JSONCompareMode.STRICT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
    }
}
