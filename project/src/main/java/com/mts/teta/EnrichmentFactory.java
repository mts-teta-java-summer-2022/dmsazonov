package com.mts.teta;

import java.util.List;

public interface EnrichmentFactory {
    String enrich(String content) throws Exception;

    List<String> getSuccessEnrichedList();

    List<String> getErrorEnrichedList();
}
