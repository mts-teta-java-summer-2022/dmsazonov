package com.mts.teta;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MsisdnMessageValidator {

    public static final String MSISDN = "msisdn";

    public JsonObject validate(String content) throws IllegalArgumentException {
        JsonObject jsonObject = new JsonParser().parse(content).getAsJsonObject();
        if (!jsonObject.has(MSISDN) ||
                jsonObject.get(MSISDN).isJsonNull() ||
                jsonObject.get(MSISDN).getAsString().isEmpty()) {
            throw new IllegalArgumentException("Field msisdn is mandatory!");
        }
        return jsonObject;
    }
}
