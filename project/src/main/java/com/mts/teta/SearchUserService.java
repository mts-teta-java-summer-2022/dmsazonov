package com.mts.teta;

import com.mts.teta.model.User;
import com.mts.teta.model.ContactInfo;

import java.util.concurrent.CopyOnWriteArraySet;

public class SearchUserService {
    private CopyOnWriteArraySet<ContactInfo> clientSet = new CopyOnWriteArraySet() {{
        add(new ContactInfo("111", new User("Ivan", "Ivanov")));
        add(new ContactInfo("222", new User("Petr", "Petrov")));
        add(new ContactInfo("88005553535", new User("Vasya", "Ivanov")));
    }};

    public User findUser(String phoneNumber) {
        ContactInfo contactInfo = clientSet
                .parallelStream()
                .filter(s -> s.getPhoneNumber().equals(phoneNumber))
                .findFirst()
                .orElse(null);

        return contactInfo != null ? contactInfo.getUser() : null;
    }
}
