package com.mts.teta;

import com.mts.teta.model.Message;

import java.util.HashMap;

public class EnrichmentFactoryMapper {
    private final HashMap<Message.EnrichmentType, EnrichmentFactory> mapper;

    public EnrichmentFactoryMapper() {
        this.mapper = new HashMap<>();
        mapper.put(Message.EnrichmentType.MSISDN, new MsisdnEnrichmentService(new MsisdnMessageValidator(), new UserEnrichmentService(new SearchUserService()), new ResultLogService()));
    }

    public EnrichmentFactory getEnrichmentType(Message.EnrichmentType enrichmentType) {
        return mapper.get(enrichmentType);
    }

}
