package com.mts.teta;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

public class MsisdnEnrichmentService implements EnrichmentFactory {

    public static final String ENRICHMENT = "enrichment";
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    private final MsisdnMessageValidator validator;
    private final UserEnrichmentService userEnrichmentService;
    private final ResultLogService resultLogService;

    public MsisdnEnrichmentService(MsisdnMessageValidator validator, UserEnrichmentService userEnrichmentService, ResultLogService resultLogService) {
        this.validator = validator;
        this.userEnrichmentService = userEnrichmentService;
        this.resultLogService = resultLogService;
    }

    @Override
    public String enrich(String content) throws IllegalArgumentException {
        JsonObject inputValue;
        try {
            inputValue = validator.validate(content);
        } catch (Exception e) {
            resultLogService.addError(content);
            return content;
        }
        JsonElement jsonElement = userEnrichmentService.getEnrich(inputValue);

        if (jsonElement != null) {
            inputValue.add(ENRICHMENT, jsonElement);
            resultLogService.addSuccess(content);
            return GSON.toJson(inputValue);
        }
        resultLogService.addError(content);
        return content;
    }

    @Override
    public List<String> getSuccessEnrichedList() {
        return resultLogService.getSuccessEnrichedList();
    }

    @Override
    public List<String> getErrorEnrichedList() {
        return resultLogService.getErrorEnrichedList();
    }
}
