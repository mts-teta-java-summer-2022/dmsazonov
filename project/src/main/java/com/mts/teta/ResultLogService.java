package com.mts.teta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultLogService {
    private final List<String> successEnrichedList;
    private final List<String> errorEnrichedList;

    public ResultLogService() {
        successEnrichedList = Collections.synchronizedList(new ArrayList<>());
        errorEnrichedList = Collections.synchronizedList(new ArrayList<>());
    }

    public void addSuccess(String message) {
        successEnrichedList.add(message);
    }

    public void addError(String message) {
        errorEnrichedList.add(message);
    }

    public List<String> getSuccessEnrichedList() {
        return successEnrichedList;
    }

    public List<String> getErrorEnrichedList() {
        return errorEnrichedList;
    }
}
