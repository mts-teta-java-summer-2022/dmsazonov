package com.mts.teta;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mts.teta.model.User;

public class UserEnrichmentService {

    private final SearchUserService searchUserService;

    public UserEnrichmentService(SearchUserService searchUserService) {
        this.searchUserService = searchUserService;
    }

    public JsonElement getEnrich(JsonObject inputValue) {
        User user = searchUserService.findUser(inputValue.get("msisdn").getAsString());
        return user != null ? new Gson().toJsonTree(user) : null;
    }
}
