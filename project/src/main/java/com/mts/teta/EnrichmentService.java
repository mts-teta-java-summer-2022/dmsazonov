package com.mts.teta;

import com.mts.teta.model.Message;

public class EnrichmentService {

    private final EnrichmentFactoryMapper enrichmentFactoryMapper;

    public EnrichmentService(EnrichmentFactoryMapper enrichmentFactoryMapper) {
        this.enrichmentFactoryMapper = enrichmentFactoryMapper;
    }

    public String enrich(Message message) throws Exception {
        EnrichmentFactory enrichmentFactory = enrichmentFactoryMapper.getEnrichmentType(message.getEnrichmentType());
        return enrichmentFactory.enrich(message.getContent());
    }
}
