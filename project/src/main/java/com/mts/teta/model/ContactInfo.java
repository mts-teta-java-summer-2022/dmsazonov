package com.mts.teta.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ContactInfo {
    private final String phoneNumber;
    private final User user;
}
