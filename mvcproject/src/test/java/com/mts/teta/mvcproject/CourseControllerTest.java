package com.mts.teta.mvcproject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mts.teta.mvcproject.controller.CourseController;
import com.mts.teta.mvcproject.controller.mapper.CourseMapper;
import com.mts.teta.mvcproject.controller.mapper.LesslonMapper;
import com.mts.teta.mvcproject.controller.model.CourseRequestToCreate;
import com.mts.teta.mvcproject.controller.model.CourseRequestToUpdate;
import com.mts.teta.mvcproject.model.Course;
import com.mts.teta.mvcproject.service.CourseService;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
@WebMvcTest(CourseController.class)
@ComponentScan(basePackageClasses = {CourseMapper.class, LesslonMapper.class})
class CourseControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;
    @MockBean
    CourseService courseService;
    Course course_1 = new Course(1L, "Author 1", "Title One");
    Course course_2 = new Course(2L, "Author 2", "Title Two");
    Course course_3 = new Course(3L, "Author 3", "Title Three");

    Course course_1_ru = new Course(1L, "Author 1", "Корректный заголовок");

    @Test
    public void getAllCourses_success() throws Exception {
        List<Course> courses = new ArrayList<>(Arrays.asList(course_1, course_2, course_3));
        Mockito.when(courseService.findAllCourses()).thenReturn(courses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/course")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].author", is("Author 3")));
    }

    @Test
    public void updateCourse_success() throws Exception {
        Mockito.doNothing().when(courseService).updateCourse(1L, "author", "Title");
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToUpdate("author", "Title"));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/course/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void updateCourse_validation_not_found() throws Exception {
        Mockito.doThrow(new NoSuchElementException("No value present")).when(courseService).updateCourse(1L, "author", "Title");
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToUpdate("author", "Title"));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/course/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());
    }

    @Test
    public void updateCourse_validation_error_title() throws Exception {
        Mockito.doNothing().when(courseService).updateCourse(1L, "author", "Title");
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToUpdate("author", null));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/course/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());;
    }

    @Test
    public void updateCourse_validation_error_author() throws Exception {
        Mockito.doNothing().when(courseService).updateCourse(1L, "author", "title");
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToUpdate(null, "Title"));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/course/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());
    }

    @Test
    public void deleteCourse_success() throws Exception {
        Mockito.doNothing().when(courseService).deleteCourse(1L);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/course/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCourse_validation_not_found() throws Exception {
        Mockito.doThrow(new NoSuchElementException("No value present")).when(courseService).deleteCourse(1L);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/course/1"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());
    }

    @Test
    public void createCourse_success() throws Exception {
        Mockito.when(courseService.saveCourse(new Course(course_1.getAuthor(), course_1.getTitle()))).thenReturn(course_1);
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToCreate(course_1.getAuthor(), course_1.getTitle()));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/course")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("author", is("Author 1")))
                .andExpect(jsonPath("title", is("Title One")));
    }

    @Test
    public void createCourse_success_ru() throws Exception {
        Mockito.when(courseService.saveCourse(new Course(course_1_ru.getAuthor(), course_1_ru.getTitle()))).thenReturn(course_1_ru);
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToCreate(course_1_ru.getAuthor(), course_1_ru.getTitle()));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/course")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("author", is("Author 1")))
                .andExpect(jsonPath("title", is("Корректный заголовок")));
    }

    @Test
    public void createCourse_validation_error_author() throws Exception {
        Mockito.when(courseService.saveCourse(new Course(course_1.getAuthor(), course_1.getTitle()))).thenReturn(course_1);
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToCreate(course_1.getAuthor(), null));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/course")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());
    }

    @Test
    public void createCourse_validation_error_title() throws Exception {
        Mockito.when(courseService.saveCourse(new Course(course_1.getAuthor(), course_1.getTitle()))).thenReturn(course_1);
        Gson gson = new Gson();
        String json = gson.toJson(new CourseRequestToCreate(null, "title"));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/course")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("dateOccurred").isNotEmpty())
                .andExpect(jsonPath("message").isNotEmpty());;
    }

    @Test
    public void getCoursesByTitlePrefix_success() throws Exception {
        List<Course> courses = new ArrayList<>(Arrays.asList(course_1, course_2, course_3));
        Mockito.when(courseService.findByTitleWithPrefix("Ti")).thenReturn(courses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/course/filter?titlePrefix=Ti")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].author", is("Author 3")));
    }
}
