package com.mts.teta.mvcproject;

import com.mts.teta.mvcproject.controller.validator.TitleCaseValidator;
import com.mts.teta.mvcproject.controller.validator.TitleValidatorType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TitleCaseValidatorTest {

    TitleCaseValidator validator = new TitleCaseValidator();

    private void setEn() {
        validator.setValidationType(TitleValidatorType.EN);
    }

    private void setRu() {
        validator.setValidationType(TitleValidatorType.RU);
    }

    private void setAny() {
        validator.setValidationType(TitleValidatorType.ANY);
    }
    // ----------------------TEST ENG--------------------------

    @Test
    void assert_en_correct(){
        setEn();
        assertTrue(validator.isValid("The Best Title", null));
    }


    @Test
    void assert_en_correct_one_word(){
        setEn();
        assertTrue(validator.isValid("Supertitle", null));
    }

    @Test
    void assert_en_correct_with_symbol(){
        setEn();
        assertTrue(validator.isValid("The: Best, Title", null));
    }

    @Test
    void assert_en_correct_with_symbol2(){
        setEn();
        assertTrue(validator.isValid("\"Title\"", null));
    }

    @Test
    void assert_en_correct_with_symbol3(){
        setEn();
        assertTrue(validator.isValid("'The: Best', Title", null));
    }

    @Test
    void assert_en_correct_with_special(){
        setEn();
        assertTrue(validator.isValid("The Best Title or Tittle", null));
    }

    @Test
    void assert_en_correct_with_special_all(){
        setEn();
        assertTrue(validator.isValid("The Best Title or a but for or not the an Tittle", null));
    }

    @Test
    void assert_en_incorrect_var_letters(){
        setEn();
        assertFalse(validator.isValid("This is a wonderful день", null));
    }

    @Test
    void assert_en_incorrect_small_letters(){
        setEn();
        assertFalse(validator.isValid("This is a wonderful", null));
    }

    @Test
    void assert_en_incorrect_long_sep(){
        setEn();
        assertFalse(validator.isValid("The  Best Title", null));
    }

    @Test
    void assert_en_incorrect_sep_begin(){
        setEn();
        assertFalse(validator.isValid(" The Best Title", null));
    }

    @Test
    void assert_en_incorrect_sep_end(){
        setEn();
        assertFalse(validator.isValid("The Best Title ", null));
    }

    @Test
    void assert_en_incorrect_var_register(){
        setEn();
        assertFalse(validator.isValid("ThE BeSt TiTle ", null));
    }

    @Test
    void assert_en_incorrect_forbidden_symbol(){
        setEn();
        assertFalse(validator.isValid("Th@e Best! Title", null));
    }

    @Test
    void assert_en_incorrect_with_r(){
        setEn();
        assertFalse(validator.isValid("The Best Title or \r" +
                "not the an Tittle", null));
    }

    @Test
    void assert_en_incorrect_with_t(){
        setEn();
        assertFalse(validator.isValid("The Best Title or \t" +
                "not the an Tittle", null));
    }

    @Test
    void assert_en_incorrect_with_n(){
        setEn();
        assertFalse(validator.isValid("The Best Title or \n" +
                "not the an Tittle", null));
    }

    // ----------------------TEST RU--------------------------

    @Test
    void assert_ru_correct(){
        setRu();
        assertTrue(validator.isValid("Это корректный заголовок", null));
    }


    @Test
    void assert_ru_correct_one_word(){
        setRu();
        assertTrue(validator.isValid("Суперзаголовок", null));
    }

    @Test
    void assert_ru_correct_with_symbol(){
        setRu();
        assertTrue(validator.isValid("Это: корректный, заголовок", null));
    }

    @Test
    void assert_ru_correct_with_symbol2(){
        setRu();
        assertTrue(validator.isValid("\"Суперзаголовок\"", null));
    }

    @Test
    void assert_ru_correct_with_symbol3(){
        setRu();
        assertTrue(validator.isValid("'Это: корректный', заголовок", null));
    }

    @Test
    void assert_ru_incorrect_var_letters(){
        setRu();
        assertFalse(validator.isValid("This is a wonderful день", null));
    }

    @Test
    void assert_ru_incorrect_big_letters(){
        setRu();
        assertFalse(validator.isValid("Это Некорректно", null));
    }

    @Test
    void assert_ru_incorrect_long_sep(){
        setRu();
        assertFalse(validator.isValid("Это  тоже", null));
    }

    @Test
    void assert_ru_incorrect_sep_begin(){
        setRu();
        assertFalse(validator.isValid(" И это тоже", null));
    }

    @Test
    void assert_ru_incorrect_sep_end(){
        setRu();
        assertFalse(validator.isValid("И это тоже ", null));
    }

    @Test
    void assert_ru_incorrect_var_register(){
        setRu();
        assertFalse(validator.isValid("ЛУЧШий Заголовок ", null));
    }

    @Test
    void assert_ru_incorrect_forbidden_symbol(){
        setRu();
        assertFalse(validator.isValid("Неправ@льный Заг/оловок", null));
    }

    @Test
    void assert_ru_incorrect_eng_with_r(){
        setRu();
        assertFalse(validator.isValid("Заголовок \r" +
                "некорректен", null));
    }

    @Test
    void assert_ru_incorrect_eng_with_t(){
        setRu();
        assertFalse(validator.isValid("Заголовок \t" +
                "некорректен", null));
    }

    @Test
    void assert_ru_incorrect_with_n(){
        setRu();
        assertFalse(validator.isValid("Заголовок \n" +
                "некорректен", null));
    }

    // ----------------------TEST ANY--------------------------

    @Test
    void assert_any_correct(){
        setAny();
        assertTrue(validator.isValid("The Best Title", null));
        assertTrue(validator.isValid("Это корректный заголовок", null));
    }


    @Test
    void assert_any_correct_one_word(){
        setAny();
        assertTrue(validator.isValid("Supertitle", null));
        assertTrue(validator.isValid("Суперзаголовок", null));
    }

    @Test
    void assert_any_correct_with_symbol(){
        setAny();
        assertTrue(validator.isValid("The: Best, Title", null));
        assertTrue(validator.isValid("Это: корректный, заголовок", null));
    }

    @Test
    void assert_any_correct_with_symbol2(){
        setAny();
        assertTrue(validator.isValid("\"Title\"", null));
        assertTrue(validator.isValid("\"Суперзаголовок\"", null));
    }

    @Test
    void assert_any_correct_with_symbol3(){
        setAny();
        assertTrue(validator.isValid("'The: Best', Title", null));
        assertTrue(validator.isValid("'Это: корректный', заголовок", null));
    }

    @Test
    void assert_any_correct_with_special(){
        setAny();
        assertTrue(validator.isValid("The Best Title or Tittle", null));
    }

    @Test
    void assert_any_correct_with_special_all(){
        setAny();
        assertTrue(validator.isValid("The Best Title or a but for or not the an Tittle", null));
    }

    @Test
    void assert_any_incorrect_big_letters(){
        setAny();
        assertFalse(validator.isValid("Это Некорректно", null));
    }

    @Test
    void assert_any_incorrect_var_letters(){
        setAny();
        assertFalse(validator.isValid("This is a wonderful день", null));
    }

    @Test
    void assert_any_incorrect_small_letters(){
        setAny();
        assertFalse(validator.isValid("This is a wonderful", null));
    }

    @Test
    void assert_any_incorrect_long_sep(){
        setAny();
        assertFalse(validator.isValid("The  Best Title", null));
        assertFalse(validator.isValid("Это  тоже", null));
    }

    @Test
    void assert_any_incorrect_sep_begin(){
        setAny();
        assertFalse(validator.isValid(" The Best Title", null));
        assertFalse(validator.isValid(" Это тоже", null));
    }

    @Test
    void assert_any_incorrect_sep_end(){
        setAny();
        assertFalse(validator.isValid("The Best Title ", null));
        assertFalse(validator.isValid("Это тоже ", null));
    }

    @Test
    void assert_any_incorrect_var_register(){
        setAny();
        assertFalse(validator.isValid("ThE BeSt TiTle ", null));
        assertFalse(validator.isValid("ЭтО тОже ", null));
    }

    @Test
    void assert_any_incorrect_forbidden_symbol(){
        setAny();
        assertFalse(validator.isValid("Th@e Best! Title", null));
        assertFalse(validator.isValid("Эт@ т!же1", null));
    }

    @Test
    void assert_any_incorrect_with_r(){
        setAny();
        assertFalse(validator.isValid("The Best Title or \r" +
                "not the an Tittle", null));
        assertFalse(validator.isValid("Это \r" +
                "тоже", null));
    }

    @Test
    void assert_any_incorrect_with_t(){
        setEn();
        assertFalse(validator.isValid("The Best Title or \t" +
                "not the an Tittle", null));
        assertFalse(validator.isValid("Это \t" +
                "тоже", null));
    }

    @Test
    void assert_any_incorrect_with_n(){
        setAny();
        assertFalse(validator.isValid("The Best Title or \n" +
                "not the an Tittle", null));
        assertFalse(validator.isValid("Это \n" +
                "тоже", null));
    }

}
