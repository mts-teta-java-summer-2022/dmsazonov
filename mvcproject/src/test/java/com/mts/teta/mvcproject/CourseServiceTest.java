package com.mts.teta.mvcproject;

import com.mts.teta.mvcproject.data.CourseRepository;
import com.mts.teta.mvcproject.model.Course;
import com.mts.teta.mvcproject.service.CourseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceTest {
    @Mock
    CourseRepository courseRepository;

    @InjectMocks
    CourseService courseService;

    @Test
    public void saveCourse_success(){
        Course courseRequest = new Course("Author", "Title");
        Course course = new Course(1L,"Author", "Title");
        when(courseRepository.save(any(Course.class))).thenReturn(course);

        Course createdCourse = courseService.saveCourse(courseRequest);
        assertThat(createdCourse.getAuthor()).isSameAs(course.getAuthor());
        assertThat(createdCourse.getTitle()).isSameAs(course.getTitle());
        assertThat(1L).isSameAs(course.getId());
    }
}
