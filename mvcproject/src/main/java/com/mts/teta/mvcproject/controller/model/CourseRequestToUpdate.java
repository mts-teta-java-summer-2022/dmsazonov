package com.mts.teta.mvcproject.controller.model;

import com.mts.teta.mvcproject.controller.validator.TitleCase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
public class CourseRequestToUpdate {
    @NotBlank(message = "Course author has to be filled")
    private String author;
    @TitleCase
    private String title;
}
