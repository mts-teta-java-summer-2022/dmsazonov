package com.mts.teta.mvcproject.controller.mapper;

import com.mts.teta.mvcproject.controller.model.LessonDto;
import com.mts.teta.mvcproject.model.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface LesslonMapper {

    @Mapping(target="courseId", source = "lesson.course.id")
    LessonDto getDto(Lesson lesson);
}
