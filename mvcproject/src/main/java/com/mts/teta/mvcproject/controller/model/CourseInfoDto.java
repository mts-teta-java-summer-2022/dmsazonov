package com.mts.teta.mvcproject.controller.model;

import com.mts.teta.mvcproject.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseInfoDto {
    private Long id;
    private String author;
    private String title;
    private Set<UserDto> users;
}
