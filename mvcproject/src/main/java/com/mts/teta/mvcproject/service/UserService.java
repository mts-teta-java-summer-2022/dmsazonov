package com.mts.teta.mvcproject.service;

import com.mts.teta.mvcproject.controller.model.UserRequest;
import com.mts.teta.mvcproject.data.RoleRepository;
import com.mts.teta.mvcproject.data.UserRepository;
import com.mts.teta.mvcproject.model.Role;
import com.mts.teta.mvcproject.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<User> findAllUsers(){
        return userRepository.findAll();
    }

    public User saveUser(UserRequest userRequest){
        User user = new User();
        user.setUserName(userRequest.getUserName());
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        return userRepository.save(user);
    }

    public void deleteUser(Long userId){
        User user = userRepository.findById(userId).orElseThrow();
        userRepository.delete(user);
    }

    public Set<Role> getUserRoles(Long userId){
        User user = userRepository.findById(userId).orElseThrow();
        return user.getRoles();
    }

    public Set<Role> addUserRole(Long userId, String roleName){
        User user = userRepository.findById(userId).orElseThrow();
        Role role = roleRepository.findByName(roleName).orElseThrow();
        Set<Role> roles = user.getRoles();
        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);
        return user.getRoles();
    }

    public Set<Role> deleteUserRole(Long userId, String roleName){
        User user = userRepository.findById(userId).orElseThrow();
        Role role = roleRepository.findByName(roleName).orElseThrow();
        Set<Role> roles = user.getRoles();
        roles.remove(role);
        user.setRoles(roles);
        userRepository.save(user);
        return user.getRoles();
    }
}
