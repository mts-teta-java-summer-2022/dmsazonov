package com.mts.teta.mvcproject.controller.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.regex.Pattern;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {

    public static final String[] SPECIAL = new String[]{"a", "but", "for", "or", "not", "the", "an"};
    public static final String[] SYMBOL = new String[]{"\"", "\'", ",", ":"};
    private TitleValidatorType validationType;

    public void setValidationType(TitleValidatorType type){
        this.validationType = type;
    }

    @Override
    public void initialize(TitleCase titleCase) {
        this.validationType = titleCase.titleType();
    }

    @Override
    public boolean isValid(String title, ConstraintValidatorContext constraintValidatorContext) {
        return title!=null
                &&!title.isBlank()
                && !(title.startsWith(" ") || title.endsWith(" "))
                && notContainsIncorrectSymbol(title)
                && onlyOneSpaceSeparator(title)
                && isValidLetters(title, validationType);
    }

    private boolean isValidLetters(String s, TitleValidatorType validationType) {
        return switch (validationType) {
            case RU -> isRusValidLetters(s);
            case EN -> isEngValidLetters(s);
            case ANY -> isRusValidLetters(s) || isEngValidLetters(s);
        };
    }

    private boolean notContainsIncorrectSymbol(String s) {
        return !Pattern.compile("^[\r\t\n]+$").matcher(s).matches();
    }

    private boolean onlyOneSpaceSeparator(String s) {
        char previousChar = '/';
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ' ' && c == previousChar) {
                return false;
            }
            previousChar = c;
        }
        return true;
    }

    private boolean isRusValidLetters(String s) {
        if(!Pattern.compile("^[а-яА-Я\s\"',:]+$").matcher(s).matches())
            return false;

        String[] mass = s.split(" ");
        if (!isFirstLetterUpperCase(mass[0]))
            return false;

        for (int i = 1; i < mass.length; i++) {
            if (!isAllLowerCase(mass[i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isEngValidLetters(String s) {
        if(!Pattern.compile("^[a-zA-Z\s\"',:]+$").matcher(s).matches())
            return false;

        String[] mass = s.split(" ");

        for (int i = 0; i < mass.length; i++) {
            if (!isFirstLetterUpperCase(mass[i])) {
                if (i == 0 || i == mass.length - 1 || !Arrays.asList(SPECIAL).contains(mass[i]))
                    return false;
            }
        }
        return true;
    }

    private boolean isFirstLetterUpperCase(String s) {
        s = s.replaceAll("\"", "");
        s = s.replaceAll("\'", "");
        s = s.replaceAll(",", "");
        s = s.replaceAll(":", "");
        String str = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        return s.equals(str);
    }

    private boolean isAllLowerCase(String s) {
        return s.equals(s.toLowerCase());
    }
}
