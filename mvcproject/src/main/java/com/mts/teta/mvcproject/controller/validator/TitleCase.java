package com.mts.teta.mvcproject.controller.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = TitleCaseValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD } )
@Retention(RetentionPolicy.RUNTIME)
public @interface TitleCase {
    TitleValidatorType titleType() default TitleValidatorType.ANY;

    public String message() default "Некорректное значение заголовка";
    public Class<?>[] groups() default {};
    public Class<? extends Payload>[] payload() default {};
}
