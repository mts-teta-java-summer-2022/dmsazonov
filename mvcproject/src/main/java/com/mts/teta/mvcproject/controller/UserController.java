package com.mts.teta.mvcproject.controller;

import com.mts.teta.mvcproject.controller.mapper.UserMapper;
import com.mts.teta.mvcproject.controller.model.UserDto;
import com.mts.teta.mvcproject.controller.model.UserRequest;
import com.mts.teta.mvcproject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/user")
@RequiredArgsConstructor
@Transactional
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    @GetMapping
    public List<UserDto> getUsers(){
        return userService.findAllUsers().stream()
                .map(userMapper::getDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDto createUser(@Valid @RequestBody UserRequest userRequest){
        return userMapper.getDto(userService.saveUser(userRequest));
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping("/{id}/role")
    public List<String> getUserRoles(@PathVariable Long id){
        return userService.getUserRoles(id).stream().map(r -> r.getName()).collect(Collectors.toList());
    }

    @PostMapping("/{id}/role/{roleName}")
    public List<String> addUserRole(@PathVariable Long id, @PathVariable String roleName){
        return userService.addUserRole(id, roleName).stream().map(r -> r.getName()).collect(Collectors.toList());
    }

    @DeleteMapping("/{id}/role/{roleName}")
    public List<String> deleteUserRole(@PathVariable Long id, @PathVariable String roleName){
        return userService.deleteUserRole(id, roleName).stream().map(r -> r.getName()).collect(Collectors.toList());
    }
}
