package com.mts.teta.mvcproject.controller.mapper;

import com.mts.teta.mvcproject.controller.model.UserDto;
import com.mts.teta.mvcproject.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto getDto(User user);
}
