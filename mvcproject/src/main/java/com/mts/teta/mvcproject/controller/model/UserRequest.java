package com.mts.teta.mvcproject.controller.model;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {
    @NotBlank(message = "Field userName has to be filled")
    private String userName;
    @NotBlank(message = "Field password has to be filled")
    private String password;
}
