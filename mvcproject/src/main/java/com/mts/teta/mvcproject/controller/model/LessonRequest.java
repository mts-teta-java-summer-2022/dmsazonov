package com.mts.teta.mvcproject.controller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
public class LessonRequest {
    @NotBlank(message = "Lesson title has to be filled")
    private String title;
    @NotBlank(message = "Lesson text has to be filled")
    private String text;
}
