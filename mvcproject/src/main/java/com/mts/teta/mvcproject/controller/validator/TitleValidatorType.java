package com.mts.teta.mvcproject.controller.validator;

public enum TitleValidatorType {
    RU,
    EN,
    ANY
}
