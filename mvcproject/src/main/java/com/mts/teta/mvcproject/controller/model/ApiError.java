package com.mts.teta.mvcproject.controller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;

@Setter
@Getter
@AllArgsConstructor
public class ApiError {
    OffsetDateTime dateOccurred;
    String message;
}
