package com.mts.teta.mvcproject.controller.mapper;

import com.mts.teta.mvcproject.controller.model.CourseDto;
import com.mts.teta.mvcproject.controller.model.CourseInfoDto;
import com.mts.teta.mvcproject.controller.model.CourseRequestToCreate;
import com.mts.teta.mvcproject.model.Course;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        uses = {UserMapper.class})
public interface CourseMapper {

    CourseDto getDto(Course course);

    Course getCourse(CourseRequestToCreate courseRequestToCreate);

    CourseInfoDto getInfoDto(Course course);
}
