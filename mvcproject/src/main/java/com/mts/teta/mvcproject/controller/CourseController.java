package com.mts.teta.mvcproject.controller;


import com.mts.teta.mvcproject.controller.mapper.CourseMapper;
import com.mts.teta.mvcproject.controller.mapper.LesslonMapper;
import com.mts.teta.mvcproject.controller.model.*;
import com.mts.teta.mvcproject.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNullElse;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final LesslonMapper lesslonMapper;

    @GetMapping
    public List<CourseDto> courseTable() {
        return courseService.findAllCourses()
                .stream()
                .map(courseMapper::getDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id, @Valid @RequestBody CourseRequestToUpdate request) {
        courseService.updateCourse(id, request.getAuthor(), request.getTitle());
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.deleteCourse(id);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public CourseDto createCourse(@Valid @RequestBody CourseRequestToCreate request) {
        return courseMapper.getDto(courseService.saveCourse(courseMapper.getCourse(request)));
    }

    @GetMapping("/filter")
    public List<CourseDto> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return courseService.findByTitleWithPrefix(requireNonNullElse(titlePrefix, ""))
                .stream()
                .map(courseMapper::getDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{id}/lesson")
    @ResponseStatus(code = HttpStatus.CREATED)
    public LessonDto createLesson(@PathVariable Long id, @Valid @RequestBody LessonRequest request) {
        return lesslonMapper.getDto(courseService.saveLesson(id, request));
    }

    @GetMapping("/{id}/lesson")
    @Transactional
    public List<LessonDto> getLessons(@PathVariable Long id) {
        return courseService.getById(id).getLessons().stream()
                .map(lesslonMapper::getDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}/lesson/{lessonId}")
    public void updateLesson(@PathVariable Long id, @PathVariable Long lessonId, @Valid @RequestBody LessonRequest request) {
        courseService.updateLesson(id, lessonId, request);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}/lesson/{lessonId}")
    public void deleteLesson(@PathVariable Long id, @PathVariable Long lessonId) {
        courseService.deleteLesson(id, lessonId);
    }

    @PostMapping("/{id}/assign")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void assignUser(@PathVariable("id") Long id,
                             @RequestParam("userId") Long userId) {
        courseService.assignUser(id, userId);
    }

    @DeleteMapping("/{id}/decouple")
    public void decoupleUser(@PathVariable("id") Long id,
                           @RequestParam("userId") Long userId) {
        courseService.decoupleUser(id, userId);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/{id}/info")
    public CourseInfoDto getInfo(@PathVariable("id") Long id){
        return courseMapper.getInfoDto(courseService.getById(id));
    }
}