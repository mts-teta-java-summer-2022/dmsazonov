package com.mts.teta.mvcproject.service;

import com.mts.teta.mvcproject.controller.model.LessonRequest;
import com.mts.teta.mvcproject.data.CourseRepository;
import com.mts.teta.mvcproject.data.LessonRepository;
import com.mts.teta.mvcproject.data.UserRepository;
import com.mts.teta.mvcproject.model.Course;
import com.mts.teta.mvcproject.model.Lesson;
import com.mts.teta.mvcproject.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Component
public class CourseService {
    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;
    private final UserRepository userRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository, LessonRepository lessonRepository, UserRepository userRepository) {
        this.courseRepository = courseRepository;
        this.lessonRepository = lessonRepository;
        this.userRepository = userRepository;
    }

    public List<Course> findAllCourses() {
        return courseRepository.findAll();
    }

    public Course getById(Long id) {
        return courseRepository.findById(id).orElseThrow();
    }

    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }

    public void updateCourse(Long id, String author, String title) {
        Course course = courseRepository.findById(id).orElseThrow();
        course.setAuthor(author);
        course.setTitle(title);
        courseRepository.save(course);
    }

    public void deleteCourse(Long id) {
        Course course = courseRepository.findById(id).orElseThrow();
        courseRepository.delete(course);
    }

    public List<Course> findByTitleWithPrefix(String titlePrefix) {
        return courseRepository.findByTitleStartsWith(titlePrefix);
    }

    public Lesson saveLesson(Long id, LessonRequest request) {
        Course course = courseRepository.findById(id).orElseThrow();
        Lesson lesson = new Lesson(request.getTitle(), request.getText(), course);
        return lessonRepository.save(lesson);
    }

    public Lesson updateLesson(Long id, Long lessonId, LessonRequest request) {
        Course course = courseRepository.findById(id).orElseThrow();
        Lesson lesson = course.getLessons().stream()
                .filter(l -> lessonId.equals(l.getId()))
                .findAny()
                .orElseThrow((() -> new NoSuchElementException("No lesson with id:"+ lessonId +" in course")));
        lesson.setTitle(request.getTitle());
        lesson.setText(request.getText());
        return lessonRepository.save(lesson);
    }

    public void deleteLesson(Long id, Long lessonId){
        Course course = courseRepository.findById(id).orElseThrow();
        Lesson lesson = course.getLessons().stream()
                .filter(l -> lessonId.equals(l.getId()))
                .findAny()
                .orElseThrow((() -> new NoSuchElementException("No lesson with id:"+ lessonId +" in course")));
        lessonRepository.delete(lesson);
        course.getLessons().remove(lesson);
        courseRepository.save(course);
    }

    public void assignUser(Long id, Long userId){
        User user = userRepository.findById(userId)
                .orElseThrow(NoSuchElementException::new);
        Course course = courseRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        course.getUsers().add(user);
        courseRepository.save(course);
    }

    public void decoupleUser(Long id, Long userId){
        User user = userRepository.findById(userId)
                .orElseThrow(NoSuchElementException::new);
        Course course = courseRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        course.getUsers().remove(user);
        courseRepository.save(course);
    }
}