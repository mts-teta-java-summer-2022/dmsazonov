/* 1. Количество пользователей, которые не создали ни одного поста.  */

select count(*) as cnt
from profile pr
left join post p ON p.profile_id = pr.profile_id
where p.profile_id is null;

/* cnt=5 */

/* 2. Выберите ID всех постов по возрастанию, у которых 2 комментария, title начинается с цифры, а длина content больше 20.*/

select pos.post_id from (
	select p.post_id, count(c.*) as cnt
	from post p
	join comment c on p.post_id = c.post_id
	where p.title ~ '^[0-9]' and length(p.content) > 20
	group by p.post_id) as pos
where cnt=2 order by pos.post_id;

/* post_id
 *
		22
		24
		26
		28
		32
		34
		36
		38
		42
		44
*/


/* 3. Выберите первые 3 ID постов по возрастанию, у которых либо нет комментариев, либо он один. */

select pos.post_id from (
	select p.post_id, count(c.*) as cnt
	from post p
	left join comment c on p.post_id = c.post_id
	group by p.post_id) as pos
where (cnt=0 or cnt=1) order by pos.post_id
limit 3;

/* post_id
 *
		1
		3
		5
*/

